import React, { ReactElement } from 'react';
import { Breakpoint } from 'domain/responsive/breakpoint.types';

interface Props {
  current: Breakpoint;
  mobileRenderFn: () => ReactElement<any>;
  desktopRenderFn: () => ReactElement<any>;
}

const Switcher: React.SFC<Props> = (props: Props) => (
  props.current === Breakpoint.Mobile
    ? props.mobileRenderFn()
    : props.desktopRenderFn()
);

export default Switcher;
