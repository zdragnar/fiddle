# Call to Action Button

A call-to-action is a primary interactive mechanism to inform a user of an option they may (or must) initiate.

## Rules

A call-to-action **MUST**:
* be labelled with text as the action the user will take by interacting
* fit a role of "primary", "secondary", or "caution"
* utilize the "caution" role ONLY when the action is destructive (cannot be reversed)
* utilize the "primary" role for the "most obvious next step" in common workflows
* utilize the "secondary" role for "escape hatch" or alternate workflow paths

A call to action **SHOULD**:
* display a loading icon, or have a loading icon in a relevant context, when disabled
* be the only "primary" call-to-action when appearing in a group. In cases where multiple workflow paths are equally obvious, no more than two primary calls-to-action should appear. In cases where three or more are needed, use only the secondary role

A call to action **MAY**:
* navigate to another page
* stay on the same page
* display an icon for quicker recognition
* be in a disabled state. Users should be told by surrounding context how to activate it

## Examples

### Non-navigational

```js
<div>
<Cta>Primary action</Cta>
<Cta role="secondary">Secondary action</Cta>
<Cta role="secondary" disabled>Disabled action</Cta>
<Cta role="caution">Destructive action</Cta>
</div>
```

### Navigational

```js
<Cta>This will navigate to another page</Cta>
```
