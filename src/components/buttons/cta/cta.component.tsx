import React, { Component } from 'react';
import { Link } from 'redux-first-router-link';
import { ButtonProps } from 'antd/lib/button';
import { Button } from 'antd';

export enum CtaRole {
  'Primary' = 'primary',
  'Secondary' = 'secondary',
  'Caution' = 'caution'
}

export type CtaProps = ButtonProps & Link.Props & {
  role: CtaRole;
};

export default class Cta extends Component<CtaProps> {
  static displayName = 'Cta';
  static defaultProps = {
    role: CtaRole.Primary
  };

  render() {
    return this.renderButton();
    // const to = this.props.to;

    // return (
    //   to
    //     ? this.renderLink()
    //     : this.renderButton()
    // );
  }

  // renderLink() {
  //   const {
  //     disabled, ghost, htmlType, icon, loading, shape, size, target, type, onClick,
  //     ...linkProps
  //   } = this.props;

  //   const buttonProps = {
  //     disabled, ghost, htmlType, icon, loading, shape, size, target, type, onClick
  //   };
  //   return (
  //     <Button {...buttonProps}>
  //       <Link {...linkProps}>{this.props.children}</Link>
  //     </Button>
  //   );
  // }

  protected getButtonType(role: CtaRole) {
    if (role === CtaRole.Primary) {
      return 'primary';
    } else if (role === CtaRole.Secondary) {
      return undefined;
    }
    return 'danger';
  }

  protected renderButton() {
    const {
      role,
      children,
      ...props
    } = this.props;

    return <Button type={this.getButtonType(role)} {...props}>{children}</Button>;
  }
}
