import React from 'react';

const NotFoundComponent = () => <div>This is not the page you are looking for!</div>;

export default NotFoundComponent;
