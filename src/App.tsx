import * as React from 'react';
import { connect } from 'react-redux';
import { BreakpointState } from 'redux-responsive';

import { Layout, Menu } from 'antd';

import { GlobalState } from 'store/global/global.reducer';

import Switcher from 'components/responsive/switcher.component';
import Page from 'components/page/page.component';

import './App.css';

const { Header, Content, Footer } = Layout;

const mapStoreToProps = (store: GlobalState) => ({
  browser: store.browser
});

export interface AppProps {
  browser: BreakpointState;
}

class App extends React.Component<AppProps> {
  renderMobileBlurb = () => <p><strong>MobileRules!</strong></p>;
  renderDesktopBlurb = () => <p><strong>DesktopRules!</strong></p>;
  render() {
    return (
      <Layout className="App">
        <Header>
          Header ahoy
        </Header>
        <Content>
          this is some test content in the {this.props.browser.mediaType} breakpoint.
          <Switcher
            current={this.props.browser.mediaType}
            mobileRenderFn={this.renderMobileBlurb}
            desktopRenderFn={this.renderDesktopBlurb}
          />
          <Page />
        </Content>
        <Footer>
          this is the footer
        </Footer>
      </Layout>
    );
  }
}

export default connect(mapStoreToProps)(App);
