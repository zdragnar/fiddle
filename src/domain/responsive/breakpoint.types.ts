export enum Breakpoint {
  Mobile = 'mobile',
  Desktop = 'infinity'
}
