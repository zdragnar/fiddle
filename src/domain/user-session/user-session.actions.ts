import { Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';

import AuthenticatedHttp from 'service/api/authenticated-http';
import { RoutingConstants } from 'lib/routing/constants';

import { UserSession, UserSessionState } from './user-session.types';
import UserSessionRepository from './user-session.repository';

let timer;

export const boot = (): ThunkAction<void, { session: UserSessionState }, any> => (dispatch, getState) => {
  const { session } = getState();
  if (session.error || session.isLoading || !session.state) {
    return;
  }

  return dispatch(startSession(session.state));
};

export const NewUserSessionActionType = 'new-user-session';

// this workflow assumes a sliding window for token validity
export const startSession = (session: UserSession): ThunkAction<void, { session: UserSessionState}, any> =>
  (dispatch, getState) => {
    const nextTimeout = session.expiresAt.valueOf() - 1800000;
    const wait = nextTimeout - Date.now();

    if (wait < 0) {
      dispatch({ type: RoutingConstants.LOGOUT });
      return;
    }

    timer = global.setTimeout(
      async () => {
        let newSession: UserSession | null = null;

        try {
          newSession = await UserSessionRepository.refresh(session);
        } catch (e) {
          // todo: implement token re-authorization if server implements
          //       multiple sliding windows; simply return a new invocation to `startSession`
          console.error(e);
          dispatch({ type: RoutingConstants.LOGOUT });
        }

        dispatch(setSession(newSession!));
      },
      wait
    );
  };

export const setSession = (session: UserSession) => {
  AuthenticatedHttp.setToken(session.token);
  return { type: NewUserSessionActionType, payload: session };
};
