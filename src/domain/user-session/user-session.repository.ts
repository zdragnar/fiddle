import Http from 'service/api/http';
import { UserSession } from './user-session.types';

class UserSessionRepository {
  boot() {
    if (localStorage.getItem('user-session')) {
      return new UserSession(JSON.parse(localStorage.getItem('user-session') as string));
    }
    return null;
  }

  refresh(userSession: UserSession) {
    return Http.post<UserSession>(
      'PATH_TO_REFRESH_TOKEN',
      { token: userSession.token },
      (response: { token: string, expiry: string }) => UserSession.update(
        userSession,
        { $merge: { token: response.token, expiresAt: new Date(response.expiry) } }
      )
    );
  }
}

export default new UserSessionRepository();
