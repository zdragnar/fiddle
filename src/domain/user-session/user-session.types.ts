import t from 'tcomb';
import { AsyncState } from 'lib/reducer/async-reducer';

export interface UserSession {
  id: string;
  token: string;
  expiresAt: Date;
}

export const UserSession = t.struct<UserSession>({
  id: t.String,
  token: t.String,
  expiresAt: t.Date
});

export interface UserSessionState extends AsyncState<UserSession, any> {}
