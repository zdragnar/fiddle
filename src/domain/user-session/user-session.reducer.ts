import { Action } from 'redux';

import { AsyncState } from 'lib/reducer/async-reducer';
import { UserSession } from './user-session.types';
import { NewUserSessionActionType } from './user-session.actions';

export type UserSessionState = AsyncState<UserSession, any>;

export default (s: UserSessionState, a: Action): UserSessionState => {
  let state = s;
  if (!state) {
    state = { isLoading: false, state: null, error: null };
  }

  if (a.type === NewUserSessionActionType) {
    state = Object.assign(s, { state: (a as any).payload });
  }

  return state;
};
