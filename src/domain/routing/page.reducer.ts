import { NOT_FOUND } from 'redux-first-router';
import { Action } from 'redux';

export default (state = components.HOME, action: Action) => components[action.type] || state;

const components = {
  HOME: 'home.component',
  [NOT_FOUND]: 'not-found.component'
};
