import React from 'react';
import ReactDOM from 'react-dom';
import { connectRoutes } from 'redux-first-router';
import createHistory from 'history/createBrowserHistory';
import { applyMiddleware, compose, createStore } from 'redux';
import { offline } from '@redux-offline/redux-offline';
import offlineConfig from '@redux-offline/redux-offline/lib/defaults';
import localForage from 'localforage';

import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { responsiveStoreEnhancer } from 'redux-responsive';

import routesMap from 'store/global/routes-map';
import createGlobalReducer, { GlobalState } from 'store/global/global.reducer';
import UserRepository from 'domain/user-session/user-session.repository';

import registerServiceWorker from './registerServiceWorker';

import t from 'tcomb';
import 'tcomb-form';
import en from 'tcomb-form/lib/i18n/en';
import templates from './lib/form/templates';

(t.form as any).Form.i18n = en;
(t.form as any).Form.templates = templates;

import App from './App';
import './index.css';

let composeEnhancers = compose;
if (process.env.NODE_ENV !== 'production' && (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
  composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
}

const history = createHistory();
const locationAddons = connectRoutes(history, routesMap);

const reducer = createGlobalReducer(locationAddons.reducer);
const middlewares = [
  thunk,
  locationAddons.middleware
];

if (process.env.NODE_ENV !== 'production') {
  middlewares.push(createLogger({ collapsed: true }));
}

const middleware = applyMiddleware(...middlewares);

const initialState = {
  session: {
    isLoading: false,
    state: UserRepository.boot()
  }
} as any;

// const store = createStore(reducer, initialState, compose(locationAddons.enhancer, middleware));
const store = createStore(
  reducer,
  initialState,
  composeEnhancers(
    locationAddons.enhancer,
    responsiveStoreEnhancer,
    middleware,
    offline({
      persistOptions: {
        storage: localForage
      },
      ...offlineConfig
    })
  )
);
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
