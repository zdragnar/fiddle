import React from 'react';
import t from 'tcomb';
import 'tcomb-form';

import { Form, Radio } from 'antd';

const FormItem = Form.Item;
const RadioGroup = Radio.Group;

export default function radio(locals: t.Locals<any, t.FormSelectConfig>): React.ReactNode {
  locals.config = locals.config || {};
  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement>): void {
    locals.onChange(e.target.value, [locals.path]);
  }

  return (
    <FormItem
      label={locals.label}
      validateStatus={locals.hasError ? 'error' : undefined}
      help={locals.error}
    >
      <RadioGroup onChange={onChangeHandler} value={locals.value}>
        {
          locals.config.options!.map((option: any, i) => (
            <Radio key={i} value={option.value}>{option.text || option.value}</Radio>
          ))
        }
      </RadioGroup>
    </FormItem>
  );
}
