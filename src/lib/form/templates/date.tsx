import React from 'react';
import moment, { Moment } from 'moment';
import t from 'tcomb';
import 'tcomb-form';

import { Form, DatePicker } from 'antd';

const FormItem = Form.Item;

export default function date(locals: t.Locals<Date, t.FormTextboxConfig>): React.ReactNode {
  locals.config = locals.config || {};

  const value = locals.value ? moment(locals.value) : undefined;

  function onChangeHandler(momentDate: Moment) {
    locals.onChange(momentDate.toDate(), [locals.path]);
  }
  return (
    <FormItem
      label={locals.label}
      validateStatus={locals.hasError ? 'error' : undefined}
      help={locals.error}
    >
      <DatePicker onChange={locals.onChange} {...locals.attrs as any} />
    </FormItem>
  );
}
