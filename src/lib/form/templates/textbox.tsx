import React from 'react';
import t from 'tcomb';
import 'tcomb-form';
import classnames from 'classnames';

import { Form, Input, InputNumber } from 'antd';

const FormItem = Form.Item;

export default function textbox(locals: t.Locals<string, t.FormTextboxConfig>): React.ReactNode {
  locals.config = locals.config || {};
  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    locals.onChange(e.target.value, [locals.path]);
  }
  return (
    <FormItem
      label={locals.label}
      validateStatus={locals.hasError ? 'error' : undefined}
      help={locals.error}
    >
      {
        locals.config.type === 'number'
          ? <InputNumber onChange={onChangeHandler} value={locals.value} {...locals.attrs as any} />
          :
        <Input onChange={onChangeHandler} value={locals.value} {...locals.attrs as any} />
      }
    </FormItem>
  );
}
