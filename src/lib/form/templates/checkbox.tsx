import React from 'react';
import t from 'tcomb';
import 'tcomb-form';

import { Form, Checkbox } from 'antd';

const FormItem = Form.Item;

export default function checkbox(locals: t.Locals<boolean, t.FormTextboxConfig>): React.ReactNode {
  locals.config = locals.config || {};
  const checked = !!locals.value;
  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement>): void {
    locals.onChange(e.target.checked, [locals.path]);
  }
  return (
    <FormItem
      label={locals.label}
      validateStatus={locals.hasError ? 'error' : undefined}
      help={locals.error}
    >
      <Checkbox checked={checked} onChange={onChangeHandler} {...locals.attrs as any} />
    </FormItem>
  );
}
