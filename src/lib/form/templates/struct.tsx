import React from 'react';
import t from 'tcomb';
import 'tcomb-form';

import { Form, Checkbox } from 'antd';

const FormItem = Form.Item;

export default function struct(locals: t.Locals<any, t.FormStructConfig>): React.ReactNode {
  locals.config = locals.config || {};
  const checked = !!locals.value;

  return (
    <fieldset>
      {locals.label ? <legend>{locals.label}</legend> : null}
      {
        locals.order!.map(name => locals.inputs![name])
      }
    </fieldset>
  );
}
