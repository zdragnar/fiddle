import React from 'react';
import t from 'tcomb';
import 'tcomb-form';

import { Form, Select } from 'antd';

const FormItem = Form.Item;
const Option = Select.Option;

export default function select(locals: t.Locals<any, t.FormSelectConfig>): React.ReactNode {
  locals.config = locals.config || {};
  function onChangeHandler(value: any) {
    locals.onChange(value, [locals.path]);
  }
  return (
    <FormItem
      label={locals.label}
      validateStatus={locals.hasError ? 'error' : undefined}
      help={locals.error}
    >
      <Select onSelect={onChangeHandler} value={locals.value}>
        {
          locals.options!.map((option: any, i) => (
            <Option
              key={i}
              value={option.value}
            >
              {option.text || option.value}
            </Option>
          ))
        }
      </Select>
    </FormItem>
  );
}
