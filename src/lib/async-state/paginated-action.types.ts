import { StartAction } from './thunk-action.types';

export interface PaginatedMetadata {
  offset: number;
  limit: number;
}

export interface StartPaginatedAction<M extends PaginatedMetadata> extends StartAction<M> {
  meta: M;
}

export interface FinishPaginatedAction<P, M extends PaginatedMetadata> extends StartPaginatedAction<M> {
  payload: Array<P>;
}

export interface ErrorPaginatedAction<M extends PaginatedMetadata, E = any> extends StartPaginatedAction<M> {
  error: E;
}
