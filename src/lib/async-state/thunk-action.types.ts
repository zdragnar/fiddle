import { Action } from 'redux';
import { Maybe } from '../typescript-enhancements/maybe';

export interface ThunkActionConstants {
  Start: string;
  Finish: string;
  Error: string;
}

export interface StartAction<M = any> extends Action {
  meta: Maybe<M>;
}

export interface FinishAction<P, M = any> extends StartAction<M> {
  payload: P;
}

export interface ErrorAction<E = any, M = any> extends StartAction<M> {
  error: Maybe<E>;
}
