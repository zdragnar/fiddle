import { Reducer } from 'redux';
import { createReducer } from 'redux-create-reducer';

import { Maybe } from '../typescript-enhancements/maybe';

import { ThunkActionConstants, StartAction, FinishAction, ErrorAction } from './thunk-action.types';

export interface AsyncState<P, E> {
  isLoading: boolean;
  state: Maybe<P>;
  error: Maybe<E>;
}

export interface ActionMap<S> {
  [key: string]: Reducer<S>;
}

const StartActionHandler = <P>(state: AsyncState<P, void>, action: StartAction) => ({
  isLoading: true,
  error: null,
});

const FinishActionHandler = <P>(state: AsyncState<P, void>, action: FinishAction<P>) => ({
  isLoading: false,
  state: action.payload,
  error: null
});

const ErrorActionHandler = <E>(state: AsyncState<E, any>, action: ErrorAction<E>) => ({
  isLoading: false,
  state: null,
  error: action.error
});

export const createAsyncReducer = <P, E = Error>(
  initialState: AsyncState<P, E>,
  actions: ThunkActionConstants,
  extraActions: ActionMap<P> = {}
) =>
  createReducer<AsyncState<P, E>>(initialState || {isLoading: false}, {
    [actions.Start]: StartActionHandler,
    [actions.Finish]: FinishActionHandler,
    [actions.Error]: ErrorActionHandler,
    ...extraActions
  });
