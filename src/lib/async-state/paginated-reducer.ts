import { Reducer } from 'redux';
import { createReducer } from 'redux-create-reducer';

import { Maybe } from '../typescript-enhancements/maybe';

import { ThunkActionConstants } from './thunk-action.types';
import {
  PaginatedMetadata,
  StartPaginatedAction,
  FinishPaginatedAction,
  ErrorPaginatedAction
} from './paginated-action.types';

export interface AsyncState<P, M extends PaginatedMetadata, E> {
  isLoading: boolean;
  data: {
    items: Array<P>;
    hasMore: boolean;
  };
  error: Maybe<E>;
  metadata: M;
}

export interface ActionMap<S> {
  [key: string]: Reducer<S>;
}

const StartActionHandler = <P, M extends PaginatedMetadata>(
  state: AsyncState<P, M, void>,
  action: StartPaginatedAction<M>
) => ({
  isLoading: true,
  error: null,
  state: state.data,
  metadata: action.meta
});

const FinishActionHandler = <P, M extends PaginatedMetadata>(
  state: AsyncState<P, M, void>,
  action: FinishPaginatedAction<P, M>
) => {
  const items = state.data.items.slice();
  items.splice(action.meta.offset, 0, ...action.payload);

  const newState = {
    isLoading: false,
    state: {
      items,
      hasMore: action.payload.length >= action.meta.limit
    },
    error: null,
    metadata: action.meta
  };
};

const ErrorActionHandler = <M extends PaginatedMetadata, E>(
  state: AsyncState<void, M, E>,
  action: ErrorPaginatedAction<M, E>
) => ({
  isLoading: false,
  state: state.data,
  error: action.error,
  metadata: action.meta
});

export const createAsyncReducer = <P, M extends PaginatedMetadata, E = Error>(
  initialState: AsyncState<P, M, E>,
  actions: ThunkActionConstants,
  extraActions: ActionMap<P> = {}
) =>
  createReducer<AsyncState<P, M, E>>(initialState || {isLoading: false}, {
    [actions.Start]: StartActionHandler,
    [actions.Finish]: FinishActionHandler,
    [actions.Error]: ErrorActionHandler,
    ...extraActions
  });
