import zipObject from 'lodash/zipObject';
import { Action } from 'redux';
import { StartAction, FinishAction, ErrorAction, ThunkActionConstants } from './action.types';

export interface Thunk<R, S> {
  (getState: () => S, dispatch: (action: Action ) => any): Promise<R>;
}

export interface ThunkCreator<R, S> {
  (...args: any[]): Thunk<R, S>;
}

export const simpleThunk = <R, S = {}>(
  actions: ThunkActionConstants,
  process: (...args: any[]) => Promise<R>,
  metaArgNames = [] as any[],
  cacheTest: (state: S) => R
): ThunkCreator<R, S> =>
  (...args) => (getState, dispatch) => {
    const state = getState();
    if (cacheTest && cacheTest(state)) {
      return Promise.resolve(cacheTest(state));
    }

    const meta = zipObject(metaArgNames, args);
    dispatch({ type: actions.Start, meta } as StartAction);

    return process(...args)
      .then(result => {
        dispatch({ type: actions.Finish, payload: result, meta } as FinishAction<R>);
        return result;
      })
      .catch(error => dispatch({ type: actions.Error, payload: error, meta } as ErrorAction<any>));
  };
