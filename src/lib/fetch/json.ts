export const json = <P>(path: string, requestInit: RequestInit, transform = (r: any) => r as P): Promise<P> =>
  fetch(path, requestInit)
  .then(res => res.json())
  .then(transform);
