import { Dispatch } from 'redux';
import { redirect } from 'redux-first-router';

import { RoutingConstants as R } from 'lib/routing/constants';

const routesMap = {
  [R.HOME]: '/',
  [R.LOGOUT]: {
    path: '/logout',
    thunk: (dispatch: Dispatch<void>) => dispatch(redirect({ type: R.HOME }))
  }
};

export default routesMap;
