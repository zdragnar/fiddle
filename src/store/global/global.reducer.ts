import { Reducer, combineReducers } from 'redux';
import { LocationState } from 'redux-first-router';
import { createResponsiveStateReducer, BreakpointState } from 'redux-responsive';

import pageReducer from 'domain/routing/page.reducer';
import userSessionReducer, { UserSessionState } from 'domain/user-session/user-session.reducer';

export interface GlobalState {
  browser: BreakpointState;
  location: LocationState;
  page: string;
  session: UserSessionState;
}

export default (locationReducer: Reducer<LocationState>) => combineReducers<GlobalState>({
  browser: createResponsiveStateReducer({ mobile: 768 }),
  location: locationReducer,
  page: pageReducer,
  session: userSessionReducer
});
