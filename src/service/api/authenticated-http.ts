import map from 'lodash/map';

import { json } from 'lib/fetch/json';

import { Http, RequestArguments } from './http';

export class AuthenticatedHttp extends Http {
  private token: string;

  setToken(token: string) {
    this.token = token;
  }

  getHeaders(): Headers {
    if (!this.token) {
      throw new Error('Request made prior to authentication');
    }

    const headers = super.getHeaders();
    headers.append('Bearer', this.token);

    return headers;
  }

  protected send<P>(args: RequestArguments): Promise<P> {
    const requestInit = {
      method: args.method,
      mode: 'cors',
      headers: this.getHeaders(),
    } as Partial<RequestArguments> & {
      headers: Headers
    };

    if (args.body) {
      requestInit.body = args.body;
    }
    return json(
      args.queryParams
        ? `${args.path}?${map(args.queryParams, (key, value) => `${key}=${value}`).join('&')}`
        : args.path
      ,
      requestInit,
      args.transform
    );
  }

}

export default new AuthenticatedHttp();
