import map from 'lodash/map';

import { json } from 'lib/fetch/json';

const defaultTransform = (r: any) => r;

export interface RequestArguments {
  method: 'GET' | 'POST' | 'PUT' | 'DELETE';
  path: string;
  queryParams?: { [key: string]: any };
  body?: any;
  transform?: any;
}

export class Http {
  protected getHeaders(): Headers {
    const headers = new Headers();
    return headers;
  }

  protected send<P>(args: RequestArguments): Promise<P> {
    const requestInit = {
      method: args.method,
      mode: 'cors'
    } as Partial<RequestArguments>;

    if (args.body) {
      requestInit.body = args.body;
    }
    return json(
      args.queryParams
        ? `${args.path}?${map(args.queryParams, (key, value) => `${key}=${value}`).join('&')}`
        : args.path
      ,
      requestInit,
      args.transform
    );
  }

  get<P>(path: string, queryParams?: { [key: string]: any}, transform: (a: any) => P = defaultTransform): Promise<P> {
    return this.send({
      method: 'GET',
      path,
      queryParams,
      transform
    });
  }

  post<P>(
    path: string,
    body?: any,
    queryParams?: { [key: string]: any },
    transform: (a: any) => P = defaultTransform
  ): Promise<P> {
    return this.send({
      method: 'POST',
      path,
      body,
      queryParams,
      transform
    });
  }

  put<P>(
    path: string,
    body?: any,
    queryParams?: { [key: string]: any },
    transform: (a: any
  ) => P = defaultTransform): Promise<P> {
    return this.send({
      method: 'POST',
      path,
      body,
      queryParams,
      transform
    });
  }

  delete<P>(
    path: string,
    body?: any,
    queryParams?: { [key: string]: any },
    transform: (a: any
  ) => P = defaultTransform): Promise<P> {
    return this.send({
      method: 'POST',
      path,
      body,
      queryParams,
      transform
    });
  }
}

export default new Http();
