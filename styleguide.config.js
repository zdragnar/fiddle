const defaultWebpackConfig = require('./node_modules/react-scripts/config/webpack.config.dev.js');
const rewire = require('./config-overrides');
const _ = require('lodash');

const defaultResolver = require('react-docgen').resolver
.findAllComponentDefinitions;

module.exports = {
    components: 'src/components/**/*.tsx',
    propsParser: function(...args) {
        let result = require('react-docgen-typescript').withCustomConfig('./tsconfig.json',).parse(...args);

        result.forEach(componentInfo => {
            let name = componentInfo.displayName;
            name = name.replace(/.component/g,'');
            name = _.capitalize(_.camelCase(name));
            componentInfo.displayName = name;
        });

        return result;
    },
    webpackConfig: rewire.webpack(defaultWebpackConfig)
};
