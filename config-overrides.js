const {
  rewireWebpack: rewireTypescript,
  rewireJest: rewireTypescriptJest
} = require("react-app-rewire-typescript-babel-preset");
const { injectBabelPlugin } =  require('react-app-rewired');

module.exports = {
  webpack: function(config, env) {
    let newConfig = rewireTypescript(config);

    newConfig = injectBabelPlugin(['import', {
      libraryName: 'antd',
      libraryDirectory: 'es',
      style: 'css'
    }], newConfig);

    // configure postcss
    newConfig.module.rules.push({
      test: /\.css$/,
      use: ['postcss-loader']
    });

    return newConfig;
  },
  jest: function(config) {
    return rewireTypescriptJest(config);
  }
};
