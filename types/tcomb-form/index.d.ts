import t from 'tcomb';

import { ValidationResult } from  'tcomb-validation';

declare module 'tcomb' {
  export namespace form {
    export abstract class Component<T, C> {
      abstract getTemplate: () => (locals: Locals<T, C>) => React.ReactNode;
    }
  }


  export interface FormErrorMessageFunction {
    (value: string, path: Array<string | number>, context: any): void | React.ReactNode;
  }

  export interface FormValidationFunction {
    (value: any, path: Array<string | number>, context: any): ValidationResult;
  }

  export interface FormInputTypeInfo<T = any> {
    type: t.Type<T>;
    isMaybe: boolean;
    isSubtype: boolean;
    innerType: t.Type<T | any>;
    getValidationErrorMessage: FormValidationFunction;
  }

  export interface FormInputAttrs {
    autofocus?: boolean;
    placeholder?: React.ReactNode;
    onBlur?: () => void;
    className?: string;
    style?: any;
  }

  export interface BaseFormInputConfig<T = any, C extends BaseFormInputConfig = any> {
    factory?: t.form.Component<T, C>;
    transformer?: {
      format: (v: any) => any;
      parse: (v: any) => any;
    };
    auto?: 'placeholders' | 'none';
    help?: React.ReactNode;
    error?: React.ReactNode | FormErrorMessageFunction;
    hasError?: () => boolean;
    disabled?: boolean;
    template?: FormTemplateCreatorFunction;
  }

  export interface FormTemplateCreatorFunction<C extends BaseFormInputConfig = BaseFormInputConfig> {
    (locals: C): React.ReactNode;
  }


  export interface FormStructConfig extends BaseFormInputConfig {
    order?: string[];
    inputs?: any;
    legend?: React.ReactNode;
  }

  export interface FormListConfig<IC extends BaseFormInputConfig = BaseFormInputConfig> extends BaseFormInputConfig {
    legend?: React.ReactNode;
    item?: FormInputAttrs | IC;
    i18n?: {
      add: string;
      down: string;
      optional: string;
      required: string;
      remove: string;
      up: string;
    };
    disabledAdd?: boolean;
    disableRemove?: boolean;
    disableOrder?: boolean;
  }

  export interface FormUnionConfig<UC = BaseFormInputConfig> extends BaseFormInputConfig {
    item: UC[];
  }

  export type TextboxTypes = 'color'
    | 'date'
    | 'datetime - local'
    | 'email'
    | 'month'
    | 'number'
    | 'range'
    | 'search'
    | 'tel'
    | 'time'
    | 'url'
    | 'week'
    | 'text' | 'password' | 'hidden' | 'textarea' | 'static';

  export interface FormTextboxConfig extends BaseFormInputConfig {
    type?: TextboxTypes;
  }

  export interface FormOptionConfig {
    value: string; text: string; disabled?: boolean;
  }

  export interface FormSelectConfig extends BaseFormInputConfig {
    nullOption?: FormOptionConfig;
    order?: 'asc' | 'desc';
    options?: Array<
    FormOptionConfig |
    { label: string; options: FormOptionConfig[] }
    >;
  }

  export interface FormDateConfig extends BaseFormInputConfig {
    order?: Array<'D' | 'M' | 'Y'>;
  }

  export interface FormConfig {
    fields: {
      [key: string]: BaseFormInputConfig;
    };
  }

  export interface Locals<
    T = any,
    C extends BaseFormInputConfig = BaseFormInputConfig
    > {
    typeInfo: FormInputTypeInfo<T>;
    path: string;
    isPristine: boolean;
    error?: React.ReactNode;
    hasError?: () => boolean;
    label?: React.ReactNode;
    onChange: (raw: any, path: Array<string | number>, kind?: any) => void;
    config: C;
    value?: T;
    disabled?: boolean;
    help?: React.ReactNode;
    context?: any;
    attrs?: FormInputAttrs;
    order?: any;
    inputs?: any;
    options?: any;
  }
}
