import { Breakpoint } from '../../src/domain/responsive/breakpoint.types';

declare module 'redux-responsive' {
  export interface BreakpointState {
    mediaType: Breakpoint;
    is: {
      mobile: boolean;
      infinity: boolean;
    }
  }
}
